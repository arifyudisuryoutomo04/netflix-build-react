import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyAbJKTGQJ4QQaZFqQVpsXkd4IRlypxPCns",
    authDomain: "netflix-build-react-379cb.firebaseapp.com",
    projectId: "netflix-build-react-379cb",
    storageBucket: "netflix-build-react-379cb.appspot.com",
    messagingSenderId: "92406913370",
    appId: "1:92406913370:web:be49badc6a379f55503b63"
};

// Use this to initialize the firebase App
const firebaseApp = firebase.initializeApp(firebaseConfig);

// Use these for db & auth
const db = firebaseApp.firestore();
const auth = firebase.auth();

export { auth };
export default db;

//? refrance https://stackoverflow.com/questions/70445014/module-not-found-error-package-path-is-not-exported-from-package