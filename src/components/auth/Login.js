import React, { useState } from "react";
import "./login.css";
import SignUp from "./SignUp";

function Login() {
  const [signIn, setSignIn] = useState(false);
  return (
    <>
      <div className="login">
        <div className="bg_login">
          <img
            className="logo_login"
            src="https://www.freepnglogos.com/uploads/netflix-logo-drawing-png-19.png"
            alt=""
          />
          <button className="login_button" onClick={() => setSignIn(true)}>
            Signin
          </button>
          <div className="login_gradient" />
        </div>
        <div className="login_body">
          {signIn ? (
            <SignUp />
          ) : (
            <>
              <h1>Unlimited movies, TV shows, and more.</h1>
              <h2>Watch anywhere. Cancel anytime.</h2>
              <h3>
                Ready to watch? Enter your email to create or restart your
                membership.
              </h3>
              <div className="login_input">
                <form action="">
                  <input type="email" placeholder="Email addres" />
                  <button
                    onClick={() => setSignIn(true)}
                    className="button_getstarted"
                  >
                    GET STARTED
                  </button>
                </form>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
}

export default Login;
