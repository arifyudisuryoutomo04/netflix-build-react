import React, { useRef } from "react";
import "./SignUp.css";
import { auth } from "../../firebase";

function SignUp() {
  const emailRef = useRef(null);
  const passwordRef = useRef(null);

  const register = (e) => {
    e.preventDefault();
    auth
      .createUserWithEmailAndPassword(
        emailRef.current.value,
        passwordRef.current.value
      )
      .then((authUser) => {
        // console.log(authUser);
      })
      .catch((error) => {
        alert(error.message);
      });
  };

  const signIn = (e) => {
    e.preventDefault();

    auth
      .signInWithEmailAndPassword(
        emailRef.current.value,
        passwordRef.current.value
      )
      .then((authUser) => {
        // console.log(authUser);
      })
      .catch((error) => alert(error.message));
  };
  return (
    <>
      <div className="signup">
        <form action="">
          <h1>SignIn</h1>
          <input ref={emailRef} type="email" placeholder="Email" />
          <input ref={passwordRef} type="password" placeholder="Password" />
          <button type="submit" onClick={signIn}>
            SignIn
          </button>
          <h4>
            <span className="signup_gray">New to Netflix? </span>
            <span className="signup_link" onClick={register}>
              Sign up now.
            </span>
          </h4>
        </form>
      </div>
    </>
  );
}

export default SignUp;
