import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Nav.css";

function Nav() {
  const [show, handleShow] = useState(false);
  const navigate = useNavigate();
  const profileClick = () => {
    // 👇️ navigate programmatically
    navigate("/profile");
  };
  const homepageClick = () => {
    // 👇️ navigate programmatically
    navigate("/");
  };

  const transitionNav = () => {
    if (window.scrollY > 100) {
      handleShow(true);
    } else {
      handleShow(false);
    }
  };
  useEffect(() => {
    window.addEventListener("scroll", transitionNav);
    return () => window.removeEventListener("scroll", transitionNav);
  }, []);

  return (
    <>
      <div className={`nav ${show && "nav_black"}`}>
        <div className="nav_content">
          <img
            onClick={homepageClick}
            className="nav_logo"
            src="https://www.freepnglogos.com/uploads/netflix-logo-drawing-png-19.png"
            alt=""
          />
          <img
            onClick={profileClick}
            className="nav_avatar"
            src="https://bit.ly/dan-abramov"
            alt=""
          />
        </div>
      </div>
    </>
  );
}

export default Nav;
