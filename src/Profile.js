import React from "react";
import { useSelector } from "react-redux";
import Nav from "./components/nav";
import { selectUser } from "./features/userSlice";
import { auth } from "./firebase";
import "./Profile.css";

function Profile() {
  const user = useSelector(selectUser);
  return (
    <>
      <div className="profile">
        <Nav />
        <div className="profile_body">
          <h1>Edit Profile</h1>
          <div className="profile_info">
            <img src="https://bit.ly/dan-abramov" alt="" />
            <div className="profile_details">
              <h2>{user.email}</h2>
              <div className="profile_plas">
                <h3>Plans</h3>
                <button
                  onClick={() => auth.signOut()}
                  className="profile_signout"
                >
                  Sign Out
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Profile;
