## Getting Started Frontend - React

```bash
cd netflix-build-react
```

First, install all needed dependencies:

```bash
npm install
```

Then, run the development server:

```bash
npm start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## More

application specifications

- [React](https://reactjs.org/)
- [Redux](#)
- [React-router-dom](https://www.npmjs.com/package/react-router-dom)
- [Deploy with Firebase](#)
- [etc](#)
